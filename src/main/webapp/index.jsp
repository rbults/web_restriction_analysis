<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: renebults
  Date: 13-01-18
  Time: 15:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
      <link rel="stylesheet" type="text/css" href="css/style.css">
      <title>Restriction analysis</title>

      <script src="js/lib/TextBoxCheckDNA.js" type="text/javascript"></script>

  </head>
    <body>
      <form action="${pageContext.request.contextPath}/SequenceServlet" onsubmit="return testerDNA()" method="post" id="select">
          <div>
              <h1>Restriction analysis tool</h1>
              <label>Enter sequence:</label>

              <%-- Create textArea and button for submitting--%>
              <textarea name="sequenceArea" required=required id="sequenceArea" rows="15" cols="50"></textarea>
              <input type="submit" id="alerter">
              <br />
              <br />
              <p>Choose enzymes below, for the sequence to be cut with (default is BamHI):</p>

              <%-- Create values for the restriction enzymes --%>
              <label><input type="checkbox" name="enzyme" id="BamHI" value="G_GATCC" class="enzyme"> BamHI</label>
              <label><input type="checkbox" name="enzyme" id="EcoRI" value="G_AATTC" class="enzyme"> EcoRI</label>
              <label><input type="checkbox" name="enzyme" id="SacI" value="GAGCT_C" class="enzyme"> SacI</label>
              <label><input type="checkbox" name="enzyme" id="HindIII" value="A_AGCTT" class="enzyme"> HindIII</label>
              <label><input type="checkbox" name="enzyme" id="TaqI" value="T_CGA" class="enzyme"> TaqI</label>
              <label><input type="checkbox" name="enzyme" id="NotI" value="GC_GGCCGC" class="enzyme"> NotI</label>
              <label><input type="checkbox" name="enzyme" id="SaII" value="G_TCGAC" class="enzyme"> SaII</label>
              <label><input type="checkbox" name="enzyme" id="XbaI" value="T_CTAGA" class="enzyme"> XbaI</label>
              <label><input type="checkbox" name="enzyme" id="PovII" value="CAG_CTG" class="enzyme"> PovII</label>
              <label><input type="checkbox" name="enzyme" id="SMAI" value="CCC_GGG" class="enzyme"> SMAI</label>
              <label><input type="checkbox" name="enzyme" id="HAEIII" value="GG_CC" class="enzyme"> HAEIII</label>
              <label><input type="checkbox" name="enzyme" id="HgaI" value="GA_GCG" class="enzyme"> HgaI</label>
              <label><input type="checkbox" name="enzyme" id="AluI" value="AG_CT" class="enzyme"> AluI</label>
              <label><input type="checkbox" name="enzyme" id="EcorV" value="GAT_ATC" class="enzyme"> EcorV</label>
              <label><input type="checkbox" name="enzyme" id="KpnI" value="GGTAC_C" class="enzyme"> KpnI</label>
          </div>
      </form>
    </body>
</html>
