<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Rene
  Date: 05-04-2018
  Time: 21:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <title>Restriction results</title>
</head>
<body>
    <h1>Results of the restriction analysis</h1>

    <%-- Create the headers for the table of the results --%>
    <table>
        <tr>
            <th>Fragment ID</th>
            <th>GC-percentage</th>
            <th>Molecular Weight</th>
            <th>Start location</th>
            <th>Stop location</th>
            <th>Fasta download</th>
        </tr>

        <%-- Fill the above created table --%>
        <c:forEach var="fragment" items="${fragments}" varStatus="index">
            <tr>
                <td>${index.count}                      </td>
                <td>${fragment.getGC()}                 </td>
                <td>${fragment.getMolWeight()}          </td>
                <td>${fragment.getStartLocation()}      </td>
                <td>${fragment.getStopLocation()}       </td>
                <td><a href="<c:url value="/downloadable?fragment=${index.count}"/>">Mirror</a></td>
            </tr>

        </c:forEach>
    </table>
</body>
</html>
