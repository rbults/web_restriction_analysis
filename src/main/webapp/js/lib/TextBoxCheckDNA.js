

function testerDNA()
{
    // Create variables for pattern and input
    var pattern = /[AGTCagtc]+$/;
    var input = document.getElementById("sequenceArea");
    var newtext;


    // If a header is present, skip the header in the check
    if(input.value.startsWith(">")){
        var lines = input.value.split('\n');
        lines.splice(0,1);
        newtext = lines.join('\n');

    }else{
        newtext = input.value;
    }

    // Check if input contains just DNA characters
    // and prompt user if not
    if(newtext.match(pattern)) {
        return true;

    } else {
        alert('Please input DNA characters only');
        return false;
    }
}







