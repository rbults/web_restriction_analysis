package nl.bioinf.rbults.enzymecutter.Classes;

public class StopCalculator {

    /**
     *
     * @param frag fragment sequence
     * @param dna DNA sequence
     * @return stopLocation
     */
    public int calculateStop(String frag, String dna){

        // For every fragment, calculate the stop location
        dna = dna.replace("_", "");
        int stopLocation = dna.indexOf(frag);
        return stopLocation + (frag.length() + 1);
    }
}
