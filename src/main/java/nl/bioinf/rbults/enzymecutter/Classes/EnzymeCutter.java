package nl.bioinf.rbults.enzymecutter.Classes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EnzymeCutter {
    private String dna;
    private String[] selected;

    /**
     *
     * @param DNA DNA sequence
     * @param selectedEnzymes list of all selected enzymes
     */
    public EnzymeCutter(String DNA, String[] selectedEnzymes){
        this.dna = DNA; //.replaceAll("\\s+","");
        this.selected = selectedEnzymes;
    }

    /**
     *
     * @return fragObjects
     */
    public ArrayList<FragmentObject> start(){
        String[] fragments = null;

        // Extract the header
        String header = extractHeader();

        // Create fragment objects arraylist
        ArrayList<FragmentObject> fragObjects = new ArrayList<>();

        // Create fragments by splitting on the added '_'
        fragments = digestDNA().split("_");

        // Fill the objects arraylist
        for(String frag : fragments) {
            FragmentObject object = new FragmentObject(frag, header, dna);
            fragObjects.add(object);
        }

        return fragObjects;
    }



    /**
     *
     * @return cutsites
     */
    private String digestDNA(){

        // Add '_' on every place the enzymes would cut
        String cutsites = "";
        for(String item : selected){
            String pattern = item.replace("_", "");
            Pattern pat = Pattern.compile(pattern);
            Matcher m = pat.matcher(dna);

            while(m.find()){
                cutsites = dna.replace(pattern, item);
                dna = cutsites;
            }
        }
        return cutsites;
    }

    /**
     *
     * @return header
     */
    private String extractHeader(){
        String header = "";
        StringBuilder temp = new StringBuilder();
        ArrayList<String> dnaElements = new ArrayList<>();

        // Extract the header if there is one given in the input
        if(dna.startsWith(">")){
            String[] lines = dna.split("\n");
            header = lines[0];
            dnaElements.addAll(Arrays.asList(lines));
            dnaElements.remove(0);

            for(String item : dnaElements){
                temp.append(item);
            }
            dna = temp.toString();

        }else{
            header += "Default_header\n";
        }

        return header;
    }
}
