package nl.bioinf.rbults.enzymecutter.Classes;

public class FragmentObject {
    private String fragment;
    private String header;
    private String dna;

    /**
     *
     * @param fragment fragment sequence
     * @param header header of fasta file
     * @param dna DNA sequence
     */
    FragmentObject(String fragment, String header, String dna){
        this.fragment = fragment;
        this.header = header;
        this.dna = dna;
    }

    /**
     *
     * @return gc
     */
    public Double getGC(){
        GCpercentageCalculator gc = new GCpercentageCalculator();
        return gc.calculateGC(fragment);
    }

    /**
     *
     * @return molWeight
     */
    public Double getMolWeight(){
        MolWeightCalculator mwc = new MolWeightCalculator();
        return mwc.calculateMolWeight(fragment);
    }

    /**
     *
     * @return startLocation
     */
    public int getStartLocation(){
        StartCalculator sc = new StartCalculator();
        return sc.calculateStart(fragment, dna);
    }

    /**
     *
     * @return stopLocation
     */
    public int getStopLocation(){
        StopCalculator stc = new StopCalculator();
        return stc.calculateStop(fragment, dna);
    }

    /**
     *
     * @return sequence
     */
    public String getSequence(){
        return fragment;
    }

    /**
     *
     * @return header
     */
    public String getHeader(){
        return header;
    }

}
