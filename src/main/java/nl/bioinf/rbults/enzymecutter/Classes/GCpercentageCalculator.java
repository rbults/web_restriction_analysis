package nl.bioinf.rbults.enzymecutter.Classes;

public class GCpercentageCalculator {

    /**
     *
     * @param frag fragment sequence
     * @return GCpercentage
     */
    public Double calculateGC(String frag){
        Double GCpercentage = 0.0;

        // For every fragment, calculate the GC percentage
        for (Character fragment : frag.toCharArray()) {
            double countG = 0;
            double countC = 0;
            if(fragment.equals('G')){
                countG += 1;
            }

            if(fragment.equals('C')){
                countC += 1;
            }

            GCpercentage += (countC + countG) / frag.length() * 100;
        }
        return (double) Math.round(GCpercentage * 100) / 100;
    }
}
