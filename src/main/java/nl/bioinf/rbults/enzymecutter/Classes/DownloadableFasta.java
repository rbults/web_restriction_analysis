package nl.bioinf.rbults.enzymecutter.Classes;

import java.io.*;
import java.text.MessageFormat;

public class DownloadableFasta {

    /**
     *
     * @param fragObject
     * @param fragNum
     * @return file
     */
    public File createDownloadLink(FragmentObject fragObject, int fragNum){

        // Create all content of the to be downloaded file
        String header = fragObject.getHeader();
        String sequence = fragObject.getSequence();
        String filename = MessageFormat.format("fragment_{0}.fasta", fragNum);
        File file = new File(filename);
        String newHeader = MessageFormat.format(">Fragment_number = {1} and Original_header = {0}", header, fragNum);

        // Making sure the sequence gets below the header
        String newline = System.getProperty("line.separator");

        // Write to the to be downloaded file
        try{
            FileWriter writer = new FileWriter(file);
            writer.write(newHeader);
            writer.write(newline + sequence);
            writer.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    return file;

    }
}
