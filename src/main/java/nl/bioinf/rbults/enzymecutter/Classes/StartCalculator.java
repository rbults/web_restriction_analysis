package nl.bioinf.rbults.enzymecutter.Classes;

public class StartCalculator {

    /**
     *
     * @param frag fragment sequence
     * @param dna DNA String
     * @return startLocation
     */
    public int calculateStart(String frag, String dna){

        // For every fragment, calculate the start location
        dna = dna.replace("_", "");
        int startLocation = dna.indexOf(frag);
        return startLocation + 1;
    }
}
