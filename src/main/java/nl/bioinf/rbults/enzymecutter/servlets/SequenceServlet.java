package nl.bioinf.rbults.enzymecutter.servlets;

import nl.bioinf.rbults.enzymecutter.Classes.EnzymeCutter;
import nl.bioinf.rbults.enzymecutter.Classes.FragmentObject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "SequenceServlet", urlPatterns = "/SequenceServlet")
public class SequenceServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Create new session
        HttpSession session = request.getSession();

        // Request DNA string given as input in the HTML textArea
        // and parse all selected enzymes, and select BamHI as default enzyme
        String DNA = request.getParameter("sequenceArea").toUpperCase();
        String[] selectedEnzymes;
        if(request.getParameterValues("enzyme") == null){
            selectedEnzymes = new String[1];
            selectedEnzymes[0] = "G_GATCC";
        }else{
            selectedEnzymes = request.getParameterValues("enzyme");
        }

        // Create fragments of the given sequence
        EnzymeCutter ec = new EnzymeCutter(DNA, selectedEnzymes);
        ArrayList<FragmentObject> fragments = ec.start();

        session.setAttribute("fragments", fragments);
        request.setAttribute("fragments", fragments);

        RequestDispatcher view = request.getRequestDispatcher("results.jsp");
        view.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dis = request.getRequestDispatcher("index.jsp");
        dis.forward(request, response);
    }
}
