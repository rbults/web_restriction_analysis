package nl.bioinf.rbults.enzymecutter.servlets;

import nl.bioinf.rbults.enzymecutter.Classes.DownloadableFasta;
import nl.bioinf.rbults.enzymecutter.Classes.FragmentObject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

@WebServlet(name = "DownloadServlet", urlPatterns = "/downloadable")
public class DownloadServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Retrieve current session
        HttpSession session = request.getSession();

        // Retrieve fragments from current session
        ArrayList<FragmentObject> fragmentObjects = (ArrayList<FragmentObject>) session.getAttribute("fragments");

        // Determine fragment number and appropriate sequence of the fragment
        int fragNum = Integer.parseInt(request.getParameter("fragment"));
        FragmentObject writtenseq = fragmentObjects.get(fragNum - 1);

        // Create a file to be downloaded
        DownloadableFasta df = new DownloadableFasta();
        File fasta = df.createDownloadLink(writtenseq, fragNum);

        // Create a notificationin the broswer for the incoming download
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", fasta.getName());
        response.setHeader(headerKey, headerValue);

        // create downloadable content
        OutputStream out = response.getOutputStream();
        FileInputStream in = new FileInputStream(fasta);
        byte[] buffer = new byte[4096];
        int length;
        while ((length = in.read(buffer)) > 0){
            out.write(buffer, 0, length);
        }

        // Perform some cleanup
        in.close();
        out.flush();
        fasta.delete();

        RequestDispatcher view = request.getRequestDispatcher("results.jsp");
        view.forward(request, response);
    }
}
