## Information
- Author: Rene Bults
- Student nr: 342589
- School: Hanze hogeschool
- Study: Bio-informatics
- Class: BFV3
- Supervisor: Michiel Noback


## Description
This tool is designed to digest DNA sequences with chosen enzymes. 
The tool cuts the given sequence and it returns several properties of the fragments, which
are shown in a table on the webpage. The properties are the GC-percentage, molecular weight,
start locationsand the stop locations. The tool is fit for fasta formatted text or just
raw DNA (without header). The tool will give proper error messages if something goes wrong.
For example, if the textarea is left empty, the user will be prompted that it must be filled
in. Also, for example, if the user enters a string of characters which is not just DNA,
The user will be prompted to enter just DNA letters, using a javascript alert. The tool will not
start, until the user enters a valid sequence. 

## Usage
Usage is straight forward. the user enters a sequence in the textarea, selects
with which enzymes he wants the sequence to be cut with and presses sumbit, and they get
output as soon as the tool is done. the output is a table with several properties of
the calculated fragments. The default selected enzyme is BamHI.

